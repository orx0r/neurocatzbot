import {
  Bot,
  InputFile,
  Cron,
} from './deps.ts';

const BOT_TOKEN = Deno.env.get('BOT_TOKEN');
const GROUP_NAME = Deno.env.get('GROUP_NAME');

if (!BOT_TOKEN) {
  throw new Error('env BOT_TOKEN is not set');
}

if (!GROUP_NAME) {
  throw new Error('env GROUP_NAME is not set');
}

const bot = new Bot(BOT_TOKEN);

bot.command('start', ctx => {
  ctx.reply('just try this command /cyber_mew');
});

bot.command('cyber_mew', ctx => {
  fetch('https://thiscatdoesnotexist.com/')
    .then(response => response.blob())
    .then(blob => {
      ctx.replyWithPhoto(new InputFile(blob.stream()));
    });
});

// every 5th min of 08-23 hrs
Cron('1 5 8-23 * * *', () => {
  fetch('https://thiscatdoesnotexist.com/')
    .then(response => response.blob())
    .then(blob => {
      bot.api.sendPhoto(GROUP_NAME, new InputFile(blob.stream()));
    });
});

bot.start();