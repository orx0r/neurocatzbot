FROM hayd/alpine-deno:1.9.0

# timezone
RUN set -ex \
    && apk --update --no-cache add tzdata \
    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
    && echo "Europe/Moscow" > /etc/timezone

WORKDIR /app

USER deno

# Ideally cache deps.ts will download and compile _all_ external files used in main.ts.
COPY deps.ts .
RUN deno cache deps.ts

ADD . .
# Compile the main app so that it doesn't need to be compiled each startup/entry.
RUN deno cache main.ts

CMD ["run", "--allow-env", "--allow-net", "main.ts"]

# docker build -t app . && docker run -it --init app
# deno run --allow-env --allow-net main.ts